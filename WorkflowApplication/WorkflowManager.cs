﻿using System;
using System.Collections.Generic;
using System.Activities;
using System.IO;
using System.Linq;

namespace WorkflowApp
{
    public class WorkflowManager
    {
        static Dictionary<string, object> AskValues()
        {

            Dictionary<string, object> values = new Dictionary<string, object>();
            Console.WriteLine("Уведiть кiлькiсть змiнних");
            try
            {
                int num = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < num; i++)
                {
                    Console.WriteLine("Уведiть назву змiнної (iдентично як у воркфловi)");
                    string name = Console.ReadLine();
                    Console.WriteLine("Уведiть значення змiнної");
                    string val = Console.ReadLine();
                    values[name] = val;
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                Console.ResetColor();
            }
            return values;

        }
        public void Run()
        {
            try
            {
                Activity workflow1 = new Workflow();
                WorkflowInvoker.Invoke(workflow1, AskValues());
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                Console.ResetColor();
            }
        }
        public void Test()
        {
            List<Dictionary<string, object>> parameters = new List<Dictionary<string, object>>();
            IDictionary<string, object> outputs = new Dictionary<string, object>();
            List<string> responses = new List<string>();
            Activity workflow1 = new Workflow();
            int n;
            string consoleOutput;
            var standardOutput = Console.Out;

            Console.WriteLine("Зараз ми з вами створимо набiр тестiв (test set)\n");
            Console.WriteLine("Скiльки робимо тестiв? (test case)");
            try
            {
                n = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(">Введiть перший тест-кейс\n");
                for (int i = 0; i < n; i++)
                {
                    parameters.Add(AskValues());
                    Console.WriteLine("Що ви очiкуєте на виходi? з точнiстю до пробiлу, замiст ентерiв пишiть @");

                    responses.Add(Console.ReadLine().Replace("@", Environment.NewLine));
                    Console.WriteLine(">Введiть наступний тест-кейс\n");
                }
                Console.WriteLine(">Результати\n");
                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("Тест №" + (i + 1).ToString() + '\n');
                    Console.WriteLine("Очiкуваний результат : ");
                    Console.WriteLine(responses[i]);
                    Console.WriteLine("Фактичний результат : ");
                    using (StringWriter stringWriter = new StringWriter())
                    {
                        //перенаправлення виведення консолi
                        Console.SetOut(stringWriter);

                        WorkflowInvoker.Invoke(workflow1, parameters[i]);
                        consoleOutput = stringWriter.ToString();

                        //перенаправлення виведення назад на екран
                        Console.SetOut(standardOutput);
                    }
                    Console.WriteLine(consoleOutput);
                    //прибираємо останнi 2 символи, бо там невидимi новi рядки
                    consoleOutput = consoleOutput.Remove(consoleOutput.Length - 2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(consoleOutput == responses[i] ? ">ТЕСТ ПРОЙДЕНО \n" : ">ТЕСТ ПРОВАЛЕНО \n");
                    Console.ResetColor();
                }
            }
            catch (Exception e)
            {
                Console.SetOut(standardOutput);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                Console.ResetColor();
            }
        }
        public void UndefinedTest()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            Dictionary<string, int> countAnswers = new Dictionary<string, int>();
            Activity workflow1 = new Workflow();
            var standardOutput = Console.Out;
            try
            {
                Console.WriteLine("Багатократне тестування програми на одному й тому ж тестi\nВведiть кiлbкiсть тестiв");
                int n = Convert.ToInt32(Console.ReadLine());
                string[] consoleOutput = new string[n];
                parameters = AskValues();

                for (int i = 0; i < n; i++)
                {
                    using (StringWriter stringWriter = new StringWriter())
                    {
                        //перенаправлення виведення консолi
                        Console.SetOut(stringWriter);

                        WorkflowInvoker.Invoke(workflow1, parameters);
                        consoleOutput[i] = stringWriter.ToString();

                        //перенаправлення виведення назад на екран
                        Console.SetOut(standardOutput);
                    }
                }
                for (int i = 0; i < n; i++)
                {
                    if (!countAnswers.ContainsKey(consoleOutput[i]))
                        countAnswers.Add(consoleOutput[i], 1);
                    else
                    {
                        countAnswers[consoleOutput[i]]++;
                    }
                }

                Console.ForegroundColor = ConsoleColor.Blue;
                for (int i = 0; i < countAnswers.Count; i++)
                    Console.WriteLine("{0}% тестiв повернули значення : \n{1}", countAnswers.ElementAt(i).Value * 100.0 / n, countAnswers.ElementAt(i).Key);
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.SetOut(standardOutput);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                Console.ResetColor();
            }
        }
    }
}
