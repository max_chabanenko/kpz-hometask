﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace WorkflowApp
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkflowManager workflowManager = new WorkflowManager();
            WorkflowXAMLParser parser = new WorkflowXAMLParser();
            int choice = 0;
            while (choice != 5)
            {
                Console.WriteLine("Натиснiть 1 для запуску");
                Console.WriteLine("Натиснiть 2 для переведення робочого процесу у код");
                Console.WriteLine("Натиснiть 3 для тестування");
                Console.WriteLine("Натиснiть 4 для тестування на недетермiнованiсть");
                Console.WriteLine("Натиснiть 5 для виходу");
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
                    Console.ResetColor();
                }
                if (choice == 1)
                {
                    workflowManager.Run();
                }
                if (choice == 2)
                {
                    Console.WriteLine(parser.PrintTranslation());
                }
                if (choice == 3)
                {
                    workflowManager.Test();
                }
                if (choice == 4)
                {
                    workflowManager.UndefinedTest();
                }
                if (choice == 5)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Виберiть один з запропонованих варiантiв");
                }
            }
        }
    }
}
