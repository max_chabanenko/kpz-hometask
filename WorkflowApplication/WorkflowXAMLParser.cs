﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace WorkflowApp
{
    class WorkflowXAMLParser
    {
        public string PrintTranslation()
        {
            string result = "";
            var enviroment = System.Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(enviroment).Parent.FullName;
            string path = Path.Combine(projectDirectory, "Workflow1.xaml");
            string xmlString = System.IO.File.ReadAllText(path);
            xmlString = Regex.Replace(xmlString, "&quot;", "\"");
            xmlString = Regex.Replace(xmlString, "&gt;", ">");
            xmlString = Regex.Replace(xmlString, "&lt;", "<");
            xmlString = Regex.Replace(xmlString, "(?<=\\[.*)=(?=.*\\])", "==");
            string cString = string.Empty;
            string oldline = string.Empty;
            using (StringReader reader = new StringReader(xmlString))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (Regex.IsMatch(line, ".*<x:Property Name=\".*\" Type=\"InArgument\\(x:.*\\)\" />"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + Regex.Match(line, "(?<=<x:Property Name=\".*\" Type=\"InArgument\\(x:).*(?=\\)\" />)").Value + " " + Regex.Match(line, "(?<=<x:Property Name=\").*(?=\" Type=\"InArgument\\(x:.*\\)\" />)").Value + ";\n";
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "Console.WriteLine(\"" + Regex.Match(line, "(?<=<x:Property Name=\").*(?=\" Type=\"InArgument\\(x:.*\\)\" />)").Value + " = \");\n";
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + Regex.Match(line, "(?<=<x:Property Name=\").*(?=\" Type=\"InArgument\\(x:.*\\)\" />)").Value + " = Console.ReadLine();\n";
                    }
                    else if (Regex.IsMatch(line, "<While.*Condition=\"\\[.*\\]\">"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "whileStart" + "\n";
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "while (" + Regex.Match(line, "(?<=.*<While.*Condition=\"\\[).*(?=\\]\">)").Value + ")\n";
                    }
                    else if (Regex.IsMatch(line, "</While>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "whileEnd" + "\n";
                    }
                    else if (Regex.IsMatch(line, ".*<WriteLine.*Text=\"\\[.*\\]\" />"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "Console.WriteLine(" + Regex.Match(line, "(?<=.*<WriteLine.*Text=\"\\[).*(?=\\]\" />)").Value + ");\n";
                    }
                    else if (Regex.IsMatch(line, ".* <OutArgument x:TypeArguments=\"x:Object\">\\[.*\\]</OutArgument>"))
                    {
                        oldline = line;
                    }
                    else if (Regex.IsMatch(line, "<InArgument x:TypeArguments=\"x:Object\">\\[.*\\]</InArgument>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + Regex.Match(oldline, "(?<=.*<OutArgument x:TypeArguments=\"x:Object\">\\[).*(?=\\]</OutArgument>)").Value + " = " + Regex.Match(line, "(?<=<InArgument x:TypeArguments=\"x:Object\">\\[).*(?=\\]</InArgument>)").Value + ";\n";
                    }
                    else if (Regex.IsMatch(line, ".*<WriteLine.*Text=\".*\" />"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "Console.WriteLine(\"" + Regex.Match(line, "(?<=.*<WriteLine.*Text=\").*(?=\" />)").Value + "\");\n";
                    }
                    else if (Regex.IsMatch(line, ".*<If Condition=\".*\".*>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "ifStart" + "\n";
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "if (" + Regex.Match(line, "(?<=.*<If Condition=\"\\[).*(?=\\]\".*>)").Value + ")\n";
                    }
                    else if (Regex.IsMatch(line, "</If>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "ifEnd" + "\n";
                    }
                    else if (Regex.IsMatch(line, "<If\\.Else>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length - 2)) + "else\n";
                    }
                    else if (Regex.IsMatch(line, "<Sequence .*>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "{" + "\n";
                    }
                    else if (Regex.IsMatch(line, "</Sequence>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "}" + "\n";
                    }
                    else if (Regex.IsMatch(line, "<Parallel .*>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "ParallelStart" + "\n";
                    }
                    else if (Regex.IsMatch(line, "</Parallel>"))
                    {
                        cString = cString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "ParallelEnd" + "\n";
                    }
                }
            }
            cString = Parallelize(cString);
            cString = cString.Replace("ifStart", "");
            cString = cString.Replace("ifEnd", "");
            result += "using System;\n" +
                "using System.Linq;\n" +
                "using System.IO;\n" +
                "using System.Collections.Generic;\n" +
                "using System.Text.RegularExpressions;\n" +
                "using System.Threading;\n";

            result += "namespace SampleProject\n" +
                "{\n" +
                "class Program\n" +
                "{\n" +
                "static void Main(string[] args)\n" +
                "{\n";
            result += cString;
            result += " \n}\n}\n}";
            return result;
        }
        public string Parallelize(string cString)
        {
            string parallelString = string.Empty;
            int whitespaceCount = int.MaxValue;
            using (StringReader reader = new StringReader(cString))
            {
                string line;
                bool IsParallel = false;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!IsParallel)
                    {
                        if (Regex.IsMatch(line, "ParallelStart"))
                        {
                            IsParallel = true;
                            whitespaceCount = int.MaxValue;
                        }
                        else
                        {
                            parallelString = parallelString + line + "\n";
                        }
                    }
                    else
                    {
                        if (Regex.IsMatch(line, "ParallelEnd"))
                        {
                            IsParallel = false;
                        }
                        else
                        {
                            whitespaceCount = whitespaceCount > line.Length - line.TrimStart(' ').Length ? line.Length - line.TrimStart(' ').Length : whitespaceCount;
                            if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "ifStart"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "new Thread(() => {" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "ifEnd"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "}).Start();" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "whileStart"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "new Thread(() => {" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "whileEnd"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "}).Start();" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "{"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "new Thread(() => {" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && Regex.IsMatch(line, "}"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "}).Start();" + "\n";
                            }
                            else if (line.Length - line.TrimStart(' ').Length == whitespaceCount && !Regex.IsMatch(line, "if \\(.*\\)") && !Regex.IsMatch(line, "else") && !Regex.IsMatch(line, "while \\(.*\\)"))
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + "new Thread(() => {" + line.TrimStart(' ') + "}).Start();" + "\n";
                            }
                            else
                            {
                                parallelString = parallelString + string.Concat(Enumerable.Repeat(" ", line.Length - line.TrimStart(' ').Length)) + line + "\n";
                            }
                        }
                    }
                }
            }

            return parallelString;
        }
    }
}
