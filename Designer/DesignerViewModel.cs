﻿using Microsoft.Win32;
using System;
using System.Activities;
using System.Activities.Core.Presentation;
using System.Activities.Presentation;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.Toolbox;
using System.Activities.Statements;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Input;

namespace Designer
{
    public class DesignerViewModel : INotifyPropertyChanged
    {
        private string fileName;
        private string title = "Designer";
        private WorkflowDesigner rehostedWFDesigner;

        public string Title
        {
            get
            {
                return this.title;
            }

            private set
            {
                this.title = value;
                this.NotifyChanged("Title");
            }
        }

        public string FileName
        {
            get
            {
                return this.fileName;
            }

            private set
            {
                this.fileName = value;
                this.Title = string.Format("{0} - {1}", title, this.FileName);
            }
        }
        
        public WorkflowDesigner RehostedWFDesigner
        {
            get
            {
                return this.rehostedWFDesigner;
            }

            private set
            {
                this.rehostedWFDesigner = value;
                this.NotifyChanged("WorkflowDesignerControl");
                this.NotifyChanged("WorkflowPropertyControl");
            }
        }

        public object WorkflowDesignerControl
        {
            get
            {
                return this.RehostedWFDesigner.View;
            }
        }

        public object WorkflowPropertyControl
        {
            get
            {
                return this.RehostedWFDesigner.PropertyInspectorView;
            }
        }

        public string XAML
        {
            get
            {
                if (this.RehostedWFDesigner.Text != null)
                {
                    this.RehostedWFDesigner.Flush();
                    return this.RehostedWFDesigner.Text;
                }

                return null;
            }
        }

        public object WFToolboxControl { get; private set; }
        public ICommand OpenCommand { get; set; }
        public ICommand SaveAsCommand { get; set; }
        public ICommand ExitCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        internal void NotifyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public DesignerViewModel()
        {
            (new DesignerMetadata()).Register();
            this.WFToolboxControl = CreateWFToolbox();
            this.OpenCommand = new RunCommand(this.ExecuteOpen, CanExecuteOpen);
            this.SaveAsCommand = new RunCommand(this.ExecuteSaveAs, CanExecuteSaveAs);
            this.ExitCommand = new RunCommand(this.ExecuteExit, CanExecuteExit);
            this.RehostedWFDesigner = new WorkflowDesigner();
        }


        private static void SetToolboxBitmapAttribute(
            AttributeTableBuilder builder, ResourceReader resourceReader, Type builtInActivityType)
        {
            var bitmap = GetBitmapResource(
                resourceReader,
                builtInActivityType.IsGenericType ? builtInActivityType.Name.Split('`')[0] : builtInActivityType.Name);

            if (bitmap == null)
            {
                return;
            }

            var toolBoxAttributeType = typeof(ToolboxBitmapAttribute);

            var imageType = typeof(Image);

            var constructor = toolBoxAttributeType.GetConstructor(
                BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { imageType, imageType }, null);

            var toolBoxAttribute = constructor.Invoke(new object[] { bitmap, bitmap }) as ToolboxBitmapAttribute;

            builder.AddCustomAttributes(builtInActivityType, toolBoxAttribute);
        }
        private static Bitmap GetBitmapResource(ResourceReader resourceReader, string bitmapName)
        {
            var dictionaryEnum = resourceReader.GetEnumerator();

            Bitmap bitmap = null;

            while (dictionaryEnum.MoveNext())
            {
                if (Equals(dictionaryEnum.Key, bitmapName))
                {
                    bitmap = dictionaryEnum.Value as Bitmap;

                    if (bitmap != null)
                    {
                        var pixel = bitmap.GetPixel(bitmap.Width - 1, 0);

                        bitmap.MakeTransparent(pixel);
                    }

                    break;
                }
            }

            return bitmap;
        }
        private static bool CanExecuteOpen(object obj)
        {
            return true;
        }
        private static bool CanExecuteSaveAs(object obj)
        {
            return true;
        }

        private static bool CanExecuteExit(object obj)
        {
            return true;
        }

        /// <summary>
        /// Creates a Workflow toolbox.
        /// </summary>
        /// <param name="obj"></param>
        private static ToolboxControl CreateWFToolbox()
        {
            var toolboxControl = new ToolboxControl();

            toolboxControl.Categories.Add(
                new ToolboxCategory("Control Flow")
                    {
                        new ToolboxItemWrapper(typeof(If)),
                        new ToolboxItemWrapper(typeof(Sequence)),
                        new ToolboxItemWrapper(typeof(While)),
                    });

            toolboxControl.Categories.Add(
                new ToolboxCategory("Primitives")
                    {
                        new ToolboxItemWrapper(typeof(Assign)),
                        new ToolboxItemWrapper(typeof(WriteLine)),
                    });

            return toolboxControl;
        }

        private void ExecuteOpen(object obj)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog(Application.Current.MainWindow).Value)
            {
                this.LoadWF(openFileDialog.FileName);
            }
        }

        private void ExecuteSaveAs(object obj)
        {
            var saveFileDialog = new SaveFileDialog
            {
                AddExtension = true,
                DefaultExt = "xaml",
                FileName = this.FileName,
                Filter = "xaml files (*.xaml) | *.xaml;*.xamlx| All Files | *.*"
            };

            if (saveFileDialog.ShowDialog().Value)
            {
                this.FileName = saveFileDialog.FileName;
                this.Save();
            }
        }

        private void ExecuteExit(object obj)
        {
            Application.Current.Shutdown();
        }

        private void Save()
        {
            this.rehostedWFDesigner.Save(this.FileName);
        }

        private void LoadWF(string name)
        {
            this.ResolveAssemblies(name);
            this.FileName = name;
            this.RehostedWFDesigner = new WorkflowDesigner();
            this.RehostedWFDesigner.ModelChanged += this.WorkflowDesignerModelChanged;
            this.RehostedWFDesigner.Load(name);
        }

        private void ResolveAssemblies(string name)
        {
            var references = WorkflowCLR.LoadWorkflow(name);

            var query = from reference in references.References where !reference.Loaded select reference;
            foreach (var workflowCLRAssembly in query)
            {
                this.CheckAssemblies(workflowCLRAssembly);
            }
        }

        private void CheckAssemblies(WorkflowCLRAssembly workflowCLRAssembly)
        {
            var openFileDialog = new OpenFileDialog
            {
                FileName = workflowCLRAssembly.CodeBase,
                CheckFileExists = true,
                Filter = "Assemblies (*.dll;*.exe)|*.dll;*.exe|All Files|*.*",
            };

            if (openFileDialog.ShowDialog(Application.Current.MainWindow).Value)
            {
                if (!workflowCLRAssembly.LoadWorkflow(openFileDialog.FileName))
                {
                    MessageBox.Show("Error loading assembly...");
                }
            }
        }

        private void WorkflowDesignerModelChanged(object sender, EventArgs e)
        {
            this.NotifyChanged("XAML");
        }

        public void DesignerWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void DesignerWindow_Closed(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
