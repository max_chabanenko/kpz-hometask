﻿using System.Windows;

namespace Designer
{
    public partial class DesignerWindow : Window
    {
        private readonly DesignerViewModel designerViewModel;
        public DesignerWindow()
        {
            InitializeComponent();
            this.designerViewModel = new DesignerViewModel();
            this.DataContext = this.designerViewModel;
            this.Closing += this.designerViewModel.DesignerWindow_Closing;
            this.Closed += this.designerViewModel.DesignerWindow_Closed;
        }

        private void RefreshXamlTabOnTabItemFocus(object sender, RoutedEventArgs e)
        {
            this.designerViewModel.NotifyChanged("XAML");
        }
    }
}
