﻿using System;
using System.Activities.Presentation.Metadata;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Designer
{
    public class WorkflowCLRAssembly
    {

        private Version version;
        private Assembly assembly;
        private AssemblyName assemblyName;
        private const string PublicTokenKey1 = "b77a5c561934e089";
        private const string PublicTokenKey2 = "31bf3856ad364e35";
        public string ClrNamespace { get; set; }

        public string Name
        {
            get
            { return this.assemblyName.Name; }
        }

        public Assembly Assembly
        {
            get
            { return this.assembly; }
            private set
            {
                this.assembly = value;
                LoadWFDesignerAssembly();
            }
        }

        private string Version
        {
            get
            {
                if (this.version == null)
                {
                    this.version = this.GetDefaultVersion();
                }
                return this.version.ToString();
            }
        }

        protected string Culture
        {
            get
            {
                return this.assemblyName.CultureInfo == null ? "neutral" : this.assemblyName.CultureInfo.Name;
            }
        }

        public bool Loaded
        {
            get
            { return this.Assembly != null; }
        }

        public string CodeBase
        {
            get
            { return this.assemblyName.CodeBase ?? this.assemblyName.Name + ".dll"; }
        }

        public Assembly DesignerAssembly { get; set; }

        public WorkflowCLRAssembly(string clrNamespace)
        {
            this.ClrNamespace = clrNamespace;
            this.GetAsssemblyName();
        }

        private void GetAsssemblyName()
        {
            var parts = this.ClrNamespace.Split(';');

            if (parts.Length == 2)
            {
                var namevalue = parts[1].Split('=');

                if (namevalue.Length == 2)
                {
                    var name = namevalue[1].Trim();
                    this.assemblyName = new AssemblyName(name);
                }
            }
        }

        private void LoadWFDesignerAssembly()
        {
            if (this.assembly != null && !this.IsFrameworkAssembly())
            {
                var file = new Uri(assembly.CodeBase).AbsolutePath;
                var name = file.Insert(file.LastIndexOf('.'), ".design");
                try
                {
                    this.DesignerAssembly = Assembly.LoadFile(name);

                    this.CopyWFAssemblies(Assembly);
                    this.CopyWFAssemblies(DesignerAssembly);
                    RegisterWFDesigner();
                }
                catch (FileLoadException)
                {
                }
                catch (FileNotFoundException)
                {
                }
            }
        }

        private void CopyWFAssemblies(Assembly copyAssembly)
        {
            var sourcePath = new Uri(copyAssembly.CodeBase).AbsolutePath;
            var destinationPath = Path.Combine(Path.GetDirectoryName(new Uri(this.GetType().Assembly.CodeBase).AbsolutePath), Path.GetFileName(sourcePath));
            try
            {
                File.Copy(sourcePath, destinationPath, true);
            }
            catch (Exception)
            {
            }
        }

        private void RegisterWFDesigner()
        {
            foreach (var metadataType in DesignerAssembly.GetTypes().Where(t => typeof(IRegisterMetadata).IsAssignableFrom(t)))
            {
                var metadata = Activator.CreateInstance(metadataType) as IRegisterMetadata;
                if (metadata != null)
                {
                    metadata.Register();
                }
            }
        }

        private bool IsFrameworkAssembly()
        {
            return this.Name.StartsWith("System") || this.Name.ToLowerInvariant() == "mscorlib" || this.Name.StartsWith("Microsoft.VisualBasic") || this.Name.StartsWith("Microsoft.CSharp");
        }

        private string GetNameByKey(string key)
        {
            return string.Format("{0},Version={1},Culture={2},PublicKeyToken={3}", this.Name, this.Version, this.Culture, key);
        }

        private Version GetDefaultVersion()
        {
            if (this.IsFrameworkAssembly())
            {
                return new Version(Assembly.GetExecutingAssembly().ImageRuntimeVersion.Substring(1, 3) + ".0.0");
            }

            return null;
        }

        private bool TryLoadWorkflow()
        {
            try
            {
                this.Assembly = Assembly.Load(this.assemblyName);
                return true;
            }
            catch (FileLoadException)
            {
            }
            catch (FileNotFoundException)
            {
            }

            return false;
        }

        private bool TryLoadWorkflow(string fxKey)
        {
            try
            {
                this.Assembly = Assembly.Load(this.GetNameByKey(fxKey));
                return true;
            }
            catch (FileLoadException)
            {
            }
            catch (FileNotFoundException)
            {
            }
            return false;
        }

        public bool LoadWorkflow()
        {
            return this.IsFrameworkAssembly() ? this.TryLoadWorkflow(PublicTokenKey1) || this.TryLoadWorkflow(PublicTokenKey2) : this.TryLoadWorkflow();
        }

        public bool LoadWorkflow(string fileName)
        {
            try
            {
                this.Assembly = Assembly.LoadFile(fileName);
                return true;
            }
            catch (FileLoadException)
            {
            }
            catch (FileNotFoundException)
            {
            }

            return false;
        }
    }
}
